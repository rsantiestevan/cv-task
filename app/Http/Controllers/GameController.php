<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessLogic\DeckCards;

class GameController extends Controller
{
    const TOTAL_PLAYERS = 2;

    public function index() {
        $oDeck = new DeckCards();
        //echo var_dump($oDeck);
        $oDeck->shuffleCards();
        //echo var_dump($oDeck);
    }
}
