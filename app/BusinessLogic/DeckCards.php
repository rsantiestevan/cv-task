<?php

namespace App\BusinessLogic;

class DeckCards {

    private $listOfCard;
    private $totalCards = 32;
    private $shapes = ['Square', 'Circle', 'Triangle', 'Oval'];
    private $colours = ['Red', 'Blue', 'Green', 'Yellow'];

    public function __construct() {
        $this->listOfCard = [];
        $count = 0;
        while($this->totalCards > $count ) {
            foreach($this->shapes as $shape) {
                foreach($this->colours as $colour) {
                    $count++;
                    $this->listOfCard[] = new Card($shape, $colour);
                }
            }
        }
    }

    public function shuffleCards(){
        shuffle($this->listOfCard);
    }


}