<?php

namespace App\BusinessLogic;

class Card {

    private $shape;
    private $colour;

    public function __construct($shape, $colour) {
        $this->colour = $colour;
        $this->shape = $shape;
    }

}